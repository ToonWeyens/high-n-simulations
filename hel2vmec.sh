#!/bin/bash
# This routine creates a VMEC input file from an HELENA equilibrium.
# Currently, it is hard-coded to use a perturbation in the position.  If this
# has to be changed, just adapt the lines in the 'commands_temp' file that is
# being created.
# Note: Tested with PB3D version 1.84.
if [ "$#" -lt 4 ]; then
    echo -e "Generates VMEC input file from a HELENA equilibrium"
    echo -e "\nUsage:\n$0 HELENA_DIR VMEC_DIR PERT_FILE PERT_STRENGTH VERT_SHIFT OPTIONS \n" 
    echo -e "    with HELENA_DIR:    directory of HELENA simulation results"
    echo -e "         VMEC_DIR:      directory where to output VMEC equilibria under 'input.eq_HEL_[PERT_FILE]'"
    echo -e "         PERT_FILE:     file of perturbation to apply, or 'axi' for axisymmetric"
    echo -e "         PERT_STRENGTH: strength of perturbation in absolute value [cm]"
    echo -e "         VERT_SHIFT:    vertical shift of equilibrium [m]"
    echo -e "         OPTIONS:       options to change in the VMEC input file, separated by ';'"
    echo -e "                        (for example: 'MPOL = 13; NTOR = 4')"
    echo -e ""
    echo -e "Example usage:"
    echo -e "    $0 Hmode_PHD/ped0.3/HELENA Hmode_PHD/ped0.3/VMEC pert_ripple16.dat 3 0 'MPOL = 20; NTOR=3; FTOL_ARRAY = 1.E-6, 1.E-6, 1.E-6, 1.E-10; NS_ARRAY = 19, 39, 79, 159'"
    echo -e "    $0 Hmode_PHD/ped0.5/HELENA Hmode_PHD/ped0.5/VMEC pert_ripple16.dat 3 0.5 'MPOL = 60; NTOR=4'"
    echo -e ""
    exit 1
fi

echo "Gathering HELENA information"
echo ""

HELENA_DIR=$1
VMEC_DIR=$2
PERT_FILE=$3
PERT_STRENGTH=$4
VERT_SHIFT=$5
OPTS=$6
BASE_DIR=$(pwd)

# modify VMEC input file
echo "modifying VMEC input file"
VMEC_INPUT="input.eq_HEL_$PERT_FILE"
echo ""

# check
[[ ! -f "$HELENA_DIR/fort.10" || ! -f "$HELENA_DIR/fort.12" || ! -f "$HELENA_DIR/fort.20" ]] && echo "HELENA_DIR '$HELENA_DIR' is incomplete" && exit 1

# generate normalization factors
cd $HELENA_DIR
$BASE_DIR/get_normalization_factors.sh > /dev/null

# get R_0 and B_0
R_0=$(grep -i -o 'R_0.*' 'normalization_factors.txt' | awk '{print $3}' | sed 's/,*$//g')
B_0=$(grep -i -o 'B_0.*' 'normalization_factors.txt' | awk '{print $3}' | sed 's/,*$//g')

# set up temporary directory at base
cd $BASE_DIR
TEMP_DIR=hel2vmec_TEMP_DIR
rm -rf $TEMP_DIR
mkdir -p $TEMP_DIR
cd $TEMP_DIR
mkdir Plots Scripts Data

# get PB3D and the HELENA output file
ln -s /opt/PB3D/PB3D .
ln -s $BASE_DIR/$HELENA_DIR/fort.12 eq_HEL
ln -s $BASE_DIR/$PERT_FILE .

# generate temporary PB3D input
INPUT_FILE='input_temp'
cat <<- END > $INPUT_FILE
&inputdata_PB3D
    min_n_par_X             = 50
    min_par_X               = 0.0
    max_par_X               = 2.0
    alpha                   = 0.0
    min_r_sol               = 0.0
    max_r_sol               = 1.0
    n_r_sol                 = 50
    norm_disc_prec_sol      = 3
    norm_disc_prec_eq       = 5
    plot_magn_grid          = .false.
    plot_flux_q             = .false.
    plot_resonance          = .false.
    plot_B                  = .false.
    plot_J                  = .false.
    plot_kappa              = .false.
    max_tot_mem             = 24000
    R_0                     = $R_0
    B_0                     = $B_0
/
END

# generate commands file
COMMANDS_FILE='commands_temp'
if [[ "$PERT_FILE" == 'axi' ]]; then
    echo "axisymmetric case"
cat <<- END > $COMMANDS_FILE
yes
$VERT_SHIFT
no
1
no
stop
END
else
    if [[ ! -f $BASE_DIR/$PERT_FILE ]]; then
        echo "can't find perturbation file '$PERT_FILE' in $BASE_DIR"
        rm $INPUT_FILE
        exit 1
    else
        echo "Set up perturbation case '$PERT_FILE' with absolute strength ${PERT_STRENGTH}cm"
cat <<- END > $COMMANDS_FILE
yes
$VERT_SHIFT
no
1
yes
1
1
$PERT_FILE
yes
$PERT_STRENGTH
yes
20
stop
END
    fi
fi
echo ""

# run PB3D
echo "running PB3D with --export_HEL"
echo "    Output is saved to 'PB3D.out'"
echo "    If you see a warning with 'STOP 0', this is normal"
./PB3D $INPUT_FILE eq_HEL --export_HEL < $COMMANDS_FILE > PB3D.out
echo ""
rm $COMMANDS_FILE
rm $INPUT_FILE

# change an input variable of a PB3D or POST input file.
# input: - input file
#        - variable name
#        - replacement value
change_input_var() {
    [[ $# -ne 3 ]] && err_arg 3
    # find the line of var
    var_line=$(grep -E -nr -m 1 "(^| )$2" $1)
    # find the line number of var
    var_line_nr=$(echo $var_line | cut -d : -f 1)
    # replace whole line by new line
    sed -i "${var_line_nr}s/.*/$2 = $3/" "$1"
    echo -e "    replace $2 by $3"
}

# modify VMEC input file
if [[ $# -gt 4 ]]; then
    echo "modifying VMEC input file"
    VMEC_INPUT="input.eq_HEL_$PERT_FILE"
    # split input line in ',' elements (from http://stackoverflow.com/a/918931)
    IFS=';' read -ra OPT_elem <<< "$OPTS"
    for i in "${OPT_elem[@]}"; do
        # first element is name
        var_name=$(echo $i | cut -d = -f 1 )
        
        # rest is value (no error checking!)
        val=$(echo $i | cut --complement -d = -f 1 )
        
        # change input file variable
        change_input_var "$VMEC_INPUT" "$var_name" "$val"
    done
fi
echo ""

echo "Finalizing"
mkdir -p $BASE_DIR/$VMEC_DIR
cp $VMEC_INPUT $BASE_DIR/$VMEC_DIR/$VMEC_INPUT
cd $BASE_DIR
rm -r $TEMP_DIR
echo ""

echo "VMEC input file is available in $VMEC_DIR/$VMEC_INPUT"
