#!/bin/bash
# Runs the script hel2vmec.sh for the specific cases of the Hmode for the reproduction of PHD results.
declare -a names=("0.5" "0.6" "0.7" "0.8" "0.9" "1.0" "1.5" "2.0" "3.0" "4.0" "5.0")
declare -a ripples=("0.0" "1.0" "2.0" "3.0" "4.0")
for name in "${names[@]}"
do
    for ripple in "${ripples[@]}"
    do
        echo "running script:"
        echo "    ./hel2vmec.sh DQ0_BS/ped$name/HELENA DQ0_BS/ped$name/VMEC/$ripple pert_ripple16.dat $ripple 0 'MPOL = 20; NTOR=1; NITER = 200000'"
        ./hel2vmec.sh DQ0_BS/ped$name/HELENA DQ0_BS/ped$name/VMEC/$ripple pert_ripple16.dat $ripple 0 'MPOL = 20; NTOR=1'
        mv DQ0_BS/ped${name}/VMEC/${ripple}/input.eq_HEL_pert_ripple16.dat DQ0_BS/ped${name}/VMEC/$ripple/input.DQ0_BS_ripple${ripple}_ped${name}
cat <<- END > run.pbs
#!/bin/sh
# set up job name
#PBS -N ped_${name}_ripple${ripple}
#PBS -k oe
#PBS -l nodes=1:ppn=1
#PBS -q ib_gen8
#PBS -l vmem=4000mb
#PBS -l mem=4000mb
#PBS -l walltime=96:00:00
#PBS -m a
#PBS -M toon.weyens@gmail.com

# set up general variables
. /home/ITER/weyenst/load_MPICH3.1.3_nomods.sh

# user output
echo "Job statistics:"
echo "    ID                \$PBS_JOBID"
echo "    name              \$PBS_JOBNAME"
echo "    environment       \$PBS_ENVIRONMENT"
echo "    nodefile          \$PBS_NODEFILE"
echo "    nodes             \$(cat \$PBS_NODEFILE | paste -sd ',' -)"
echo "    array ID          \$PBS_ARRAYID"
echo "    procs             \$PBS_NP"
echo "    queue             \$PBS_QUEUE"
echo "    walltime          \$PBS_WALLTIME"
echo "    submit directory  \$PBS_O_WORKDIR"
echo "    host machine      \$PBS_O_HOST"
echo "    procs per node    \$PBS_NUM_PPN"
echo "    login name        \$PBS_O_LOGNAME"
echo "    home              \$PBS_O_HOME"
echo "" 

# setup job ID and name
jobID=\$(echo \$PBS_JOBID | cut -d'.' -f 1)

# save run directory
rundir=/home/ITER/weyenst/Programs_MPICH3.1.3/VMEC_simulations/DQ0_BS/ped${name}/VMEC/${ripple}

# move to directory
cd \$rundir

# remove output
rm -f VMEC.out VMEC.err

# set local output and error and create symbolic links
loc_out=\$PBS_O_HOME/\$PBS_JOBNAME.o\$jobID
loc_err=\$PBS_O_HOME/\$PBS_JOBNAME.e\$jobID
ln -s \$loc_out \$rundir/VMEC.out
ln -s \$loc_err \$rundir/VMEC.err

# run VMEC
cp \$COMPILE_DIR/bin/xvmec2013 .
./xvmec2013 input.DQ0_BS_ripple${ripple}_ped${name}

# move output back to run directory
mv \$loc_out \$rundir/VMEC.out
mv \$loc_err \$rundir/VMEC.err
[[ -s \$rundir/VMEC.err ]] || rm \$rundir/VMEC.err

exit
END
        mv run.pbs DQ0_BS/ped${name}/VMEC/$ripple/
    done
done
