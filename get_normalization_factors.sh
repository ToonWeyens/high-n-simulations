#!/bin/bash

# find eps, BVAC and RVAC in fort.10
eps=$(grep -i -o 'eps.*' fort.10 | awk '{print $3}' | sed 's/,*$//g')
RVAC=$(grep -i -o 'RVAC.*' fort.10 | awk '{print $3}' | sed 's/,*$//g')
BVAC=$(grep -i -o 'BVAC.*' fort.10 | awk '{print $3}' | sed 's/,*$//g')

# find radius and B0 in fort.20
radius=$(grep -i -o -m 1 'radius.*' fort.20 | awk '{print $3}' | sed 's/,*$//g')
B0=$(grep -i -o 'B0.*' fort.20 | awk '{print $3}' | sed 's/,*$//g')

# calculate R_m and B_m
eps_c="($(sed 's/[eE]+\{0,1\}/*10^/g' <<<"$eps"))"
RVAC_c="($(sed 's/[eE]+\{0,1\}/*10^/g' <<<"$RVAC"))"
BVAC_c="($(sed 's/[eE]+\{0,1\}/*10^/g' <<<"$BVAC"))"
radius_c="($(sed 's/[eE]+\{0,1\}/*10^/g' <<<"$radius"))"
B0_c="($(sed 's/[eE]+\{0,1\}/*10^/g' <<<"$B0"))"
R_m=$(echo "scale=6; ($eps_c / $radius_c) * $RVAC_c" | bc)
B_m=$(echo "scale=6; $BVAC_c / $B0_c" | bc)

echo "found in files:"
echo "    eps = $eps"
echo "    BVAC = $BVAC"
echo "    RVAC = $RVAC"
echo "    radius = $radius"
echo "    B0 = $B0"
echo ""
echo "resulting in:"
echo "    R_m = $R_m"
echo "    B_m = $B_m"
echo ""

# set file name
if [[ -f 'normalization_factors.txt' ]]; then
    echo 'overwriting file "normalization_factors.txt"'
else
    echo 'writing to file "normalization_factors.txt"'
fi

cat <<- END > 'normalization_factors.txt'
# R_m = (eps /radius) * RVAC
#     = ($eps / $radius) * $RVAC
#     = $R_m
# B_m = BVAC / B0
#     = $BVAC / $B0
#     = $B_m
    R_0                     = $R_m
    B_0                     = $B_m
END
