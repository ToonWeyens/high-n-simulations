#!/bin/bash
# Runs the script hel2vmec.sh for the specific cases of the Hmode for the 2018 EPS abstract.
declare -a names=("0.3" "0.5" "0.7" "0.9" "1.1" "1.3" "1.5" "1.7" "1.9")
declare -a ripples=("0.0" "1.0" "2.0" "3.0" "4.0" "5.0" "6.0" "7.0" "8.0" "9.0" "10.0")
for name in "${names[@]}"
do
    for ripple in "${ripples[@]}"
    do
        echo "running script:"
        echo "    ./hel2vmec.sh EPS_2018/ped$name/HELENA EPS_2018/ped$name/VMEC/$ripple ripple_map_ITER.dat $ripple 0.56710 'MPOL = 23; NTOR=2; CURTOR = -1.5E+07; NS_ARRAY =  11  21  41 201; FTOL_ARRAY = 1.0E-06  1.0E-07 1.0E-10 1.0E-18'"
        ./hel2vmec.sh EPS_2018/ped$name/HELENA EPS_2018/ped$name/VMEC/$ripple ripple_map_ITER.dat $ripple 0.56710 'MPOL = 23; NTOR=2; CURTOR = -1.5E+07; NS_ARRAY =  11  21  41 201; FTOL_ARRAY = 1.0E-06  1.0E-07 1.0E-10 1.0E-18'
        mv EPS_2018/ped${name}/VMEC/${ripple}/input.eq_HEL_ripple_map_ITER.dat EPS_2018/ped${name}/VMEC/$ripple/input.EPS_2018_ripple${ripple}_ped${name}
cat <<- END > run.pbs
#!/bin/sh
# set up job name
#PBS -N ped_${name}_ripple${ripple}
#PBS -k oe
#PBS -l nodes=1:ppn=1
#PBS -q ib_gen8
#PBS -l vmem=4000mb
#PBS -l mem=4000mb
#PBS -l walltime=96:00:00
#PBS -m a
#PBS -M toon.weyens@gmail.com

# set up general variables
. /home/ITER/weyenst/load_MPICH3.1.3.sh

# user output
echo "Job statistics:"
echo "    ID                \$PBS_JOBID"
echo "    name              \$PBS_JOBNAME"
echo "    environment       \$PBS_ENVIRONMENT"
echo "    nodefile          \$PBS_NODEFILE"
echo "    nodes             \$(cat \$PBS_NODEFILE | paste -sd ',' -)"
echo "    array ID          \$PBS_ARRAYID"
echo "    procs             \$PBS_NP"
echo "    queue             \$PBS_QUEUE"
echo "    walltime          \$PBS_WALLTIME"
echo "    submit directory  \$PBS_O_WORKDIR"
echo "    host machine      \$PBS_O_HOST"
echo "    procs per node    \$PBS_NUM_PPN"
echo "    login name        \$PBS_O_LOGNAME"
echo "    home              \$PBS_O_HOME"
echo "" 

# setup name of case and of input file
NAME=ped_${name}_ripple${ripple}
INPUT=input.EPS_2018_ripple${ripple}_ped${name}

# setup job ID and name
jobID=\$(echo \$PBS_JOBID | cut -d'.' -f 1)

# save run directory
rundir=/home/ITER/weyenst/Simulations/EPS_2018/ped${name}/VMEC/${ripple}

# set up temporary run directory
tempdir=/tmp/\$NAME

# remove output
rm -f \$rundir/VMEC.out \$rundir/VMEC.err

# set local output and error and create symbolic links
loc_out=\$PBS_O_HOME/\$PBS_JOBNAME.o\$jobID
loc_err=\$PBS_O_HOME/\$PBS_JOBNAME.e\$jobID
ln -s \$loc_out \$rundir/VMEC.out
ln -s \$loc_err \$rundir/VMEC.err

# make temporary run directory and set up files
mkdir -p \$tempdir
cd \$tempdir
rsync -zvhr --append-verify \$rundir/\$INPUT .
rsync -zvhr --append-verify --copy-links \$COMPILE_DIR/bin/xvmec2013 .

# run VMEC
./xvmec2013 \$INPUT

# move output back to run directory
mv \$loc_out \$rundir/VMEC.out
mv \$loc_err \$rundir/VMEC.err
rsync -zvhr --append-verify --remove-source-files \$tempdir/* \$rundir/
[[ -s \$rundir/VMEC.err ]] || rm \$rundir/VMEC.err

exit
END
        mv run.pbs EPS_2018/ped${name}/VMEC/${ripple}/
    done
done
